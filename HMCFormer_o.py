import torch
from torch import nn, einsum
import numpy as np
import torch.nn.functional as F
import torchvision.transforms as transforms
from PIL import Image
from einops import rearrange, repeat
import random
import matplotlib.pyplot as plt
def drop_path_f(x, drop_prob: float = 0., training: bool = False):
    """Drop paths (Stochastic Depth) per sample (when applied in main path of residual blocks).

    This is the same as the DropConnect impl I created for EfficientNet, etc networks, however,
    the original name is misleading as 'Drop Connect' is a different form of dropout in a separate paper...
    See discussion: https://github.com/tensorflow/tpu/issues/494#issuecomment-532968956 ... I've opted for
    changing the layer and argument names to 'drop path' rather than mix DropConnect as a layer name and use
    'survival rate' as the argument.

    """
    if drop_prob == 0. or not training:
        return x
    keep_prob = 1 - drop_prob
    shape = (x.shape[0],) + (1,) * (x.ndim - 1)  # work with diff dim tensors, not just 2D ConvNets
    random_tensor = keep_prob + torch.rand(shape, dtype=x.dtype, device=x.device)
    random_tensor.floor_()  # binarize
    output = x.div(keep_prob) * random_tensor
    return output


class DropPath(nn.Module):
    """Drop paths (Stochastic Depth) per sample  (when applied in main path of residual blocks).
    """
    def __init__(self, drop_prob=None):
        super(DropPath, self).__init__()
        self.drop_prob = drop_prob

    def forward(self, x):
        return drop_path_f(x, self.drop_prob, self.training)



class CyclicShift(nn.Module):
    def __init__(self, displacement):
        super().__init__()
        self.displacement = displacement

    def forward(self, x):
        return torch.roll(x, shifts=(self.displacement, self.displacement), dims=(1, 2))


class Residual(nn.Module):
    def __init__(self, fn):
        super().__init__()
        self.fn = fn

    def forward(self, x, **kwargs):
        return self.fn(x, **kwargs) + x

class Residual2(nn.Module):
    def __init__(self, fn):
        super().__init__()
        self.fn = fn
        self.first_half = HalfSplit(dim=1, first_half=True)  # 对channels进行切半操作, 第一次分: first_half=True
        self.second_split = HalfSplit(dim=1, first_half=False)  # 返回输入的另外一半channesl，两次合起来才是完整的一份channels
    def forward(self, xy, **kwargs):
        x = self.first_half(xy)
        #y = self.second_split(xy)
        x_y = self.fn(xy, **kwargs)
        return x_y + x

class PreNorm(nn.Module):
    def __init__(self, dim, fn):
        super().__init__()
        self.norm = nn.LayerNorm(dim)
        self.fn = fn

    def forward(self, x, **kwargs):
        return self.fn(self.norm(x), **kwargs)


class PreNorm2(nn.Module):
    def __init__(self, dim, fn):
        super().__init__()
        self.norm = nn.LayerNorm(2*dim)
        self.fn = fn

    def forward(self, x, **kwargs):
        return self.fn(self.norm(x), **kwargs)


class FeedForward(nn.Module):
    def __init__(self, dim, hidden_dim):
        super().__init__()
        self.net = nn.Sequential(
            nn.Linear(dim, hidden_dim),
            nn.GELU(),
            nn.Linear(hidden_dim, dim),
        )

    def forward(self, x):
        return self.net(x)


def create_mask(window_size, displacement, upper_lower, left_right):
    mask = torch.zeros(window_size ** 2, window_size ** 2)

    if upper_lower:
        mask[-displacement * window_size:, :-displacement * window_size] = float('-inf')
        mask[:-displacement * window_size, -displacement * window_size:] = float('-inf')

    if left_right:
        mask = rearrange(mask, '(h1 w1) (h2 w2) -> h1 w1 h2 w2', h1=window_size, h2=window_size)
        mask[:, -displacement:, :, :-displacement] = float('-inf')
        mask[:, :-displacement, :, -displacement:] = float('-inf')
        mask = rearrange(mask, 'h1 w1 h2 w2 -> (h1 w1) (h2 w2)')

    return mask


def get_relative_distances(window_size):
    indices = torch.tensor(np.array([[x, y] for x in range(window_size) for y in range(window_size)]))
    distances = indices[None, :, :] - indices[:, None, :]
    return distances
##################################################################################################################
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

class LayerNorm0(nn.Module):
    r""" LayerNorm that supports two data formats: channels_last (default) or channels_first.
    The ordering of the dimensions in the inputs. channels_last corresponds to inputs with
    shape (batch_size, height, width, channels) while channels_first corresponds to inputs
    with shape (batch_size, channels, height, width).
    """

    def __init__(self, normalized_shape, eps=1e-6, data_format="channels_last"):
        super().__init__()
        self.weight = nn.Parameter(torch.ones(normalized_shape), requires_grad=True)
        self.bias = nn.Parameter(torch.zeros(normalized_shape), requires_grad=True)
        self.eps = eps
        self.data_format = data_format
        if self.data_format not in ["channels_last", "channels_first"]:
            raise ValueError(f"not support data format '{self.data_format}'")
        self.normalized_shape = (normalized_shape,)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        if self.data_format == "channels_last":
            return F.layer_norm(x, self.normalized_shape, self.weight, self.bias, self.eps)
        elif self.data_format == "channels_first":
            # [batch_size, channels, height, width]
            mean = x.mean(1, keepdim=True)
            var = (x - mean).pow(2).mean(1, keepdim=True)
            x = (x - mean) / torch.sqrt(var + self.eps)
            x = self.weight.to(device)[:, None, None] * x + self.bias.to(device)[:, None, None]
            return x

class Downsample(nn.Module):
    def __init__(self, dim,out_channels,stride):
        super().__init__()
        self.norm = LayerNorm0(dim, eps=1e-6, data_format="channels_first")
        self.conv1 = BN_Conv_Mish(dim, out_channels, 3,stride, 0)
    def forward(self, x):
            x = self.norm(x)
            x = self.conv1(x)
            return x


# 划分channels: dim默认为0，但是由于channnels位置在1，所以传参为1
class HalfSplit(nn.Module):
    def __init__(self, dim=0, first_half=True):
        super(HalfSplit, self).__init__()
        self.first_half = first_half
        self.dim = dim

    def forward(self, input):
        # 对input的channesl进行分半操作
        splits = torch.chunk(input, 2, dim=self.dim)        # 由于shape=[b, c, h, w],对于dim=1，针对channels
        return splits[0] if self.first_half else splits[1]  # 返回其中的一半

## 激活函数Mish
class Mish(nn.Module):
    def __init__(self):
        super(Mish, self).__init__()

    def forward(self, x):
        return x * torch.tanh(F.softplus(x))


class BN_Conv_Mish(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride, padding=0):
        super(BN_Conv_Mish, self).__init__()

        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size, stride, kernel_size // 2, bias=False)
        # 卷积层之后总会添加BatchNorm2d进行数据的归一化处理，这使得数据在进行激活函数（Mish）之前不会因为数据过大而导致网络性能的不稳定
        self.bn = nn.BatchNorm2d(out_channels)
        self.activation = Mish()

    def forward(self, x):
        x = self.conv(x)
        x = self.bn(x)
        x = self.activation(x)
        return x

class BN_Conv_Mish1(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride, padding):
        super(BN_Conv_Mish1, self).__init__()

        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size=5, padding=2, groups=in_channels)  # depthwise conv
        # 卷积层之后总会添加BatchNorm2d进行数据的归一化处理，这使得数据在进行激活函数（Mish）之前不会因为数据过大而导致网络性能的不稳定
        self.bn = nn.BatchNorm2d(out_channels)
        self.activation = Mish()

    def forward(self, x):
        x = self.conv(x)
        x = self.bn(x)
        x = self.activation(x)
        return x


# channels shuffle增加组间交流
class ChannelShuffle(nn.Module):
    def __init__(self, groups):
        super(ChannelShuffle, self).__init__()
        self.groups = groups

    def forward(self, x):
        '''Channel shuffle: [N,C,H,W] -> [N,g,C/g,H,W] -> [N,C/g,g,H,w] -> [N,C,H,W]'''
        N, C, H, W = x.size()
        g = self.groups
        return x.view(N, g, int(C / g), H, W).permute(0, 2, 1, 3, 4).contiguous().view(N, C, H, W)


class ResidualBlock(nn.Module):
    def __init__(self, chnls, inner_chnnls=None):
        super(ResidualBlock, self).__init__()
        mid_chnnls = chnls // 2
        if inner_chnnls is None:
            inner_chnnls = chnls
        self.conv1 = BN_Conv_Mish(chnls, mid_chnnls*2, 3,1,0)
        self.conv2 = BN_Conv_Mish(mid_chnnls*2, chnls*2, 3,1,0)
        self.conv3 = nn.Conv2d(chnls*2, out_channels=chnls, kernel_size=1)
        self.bn = nn.BatchNorm2d(chnls)

    def forward(self, x):
        out = self.conv1(x)
        out = self.conv2(out)
        out = Mish()(out)
        out = self.conv3(out)
        out = self.bn(out) + x
        return out#Mish()(out)

class ResidualBlock1(nn.Module):
    def __init__(self, chnls, inner_chnnls=None):
        super(ResidualBlock1, self).__init__()
        mid_chnnls = chnls//2
        if inner_chnnls is None:
            inner_chnnls = chnls
        self.conv1 = nn.Conv2d(in_channels=chnls, out_channels= chnls, kernel_size=3, padding=1)
        self.conv2 = nn.Conv2d(in_channels=chnls, out_channels=mid_chnnls *4, kernel_size=1)
        self.conv3 = nn.Conv2d(in_channels=mid_chnnls *4, out_channels=chnls, kernel_size=1)
        self.bn = nn.BatchNorm2d(chnls)

    def forward(self, x):
        out = self.conv1(x)
        out = self.conv2(out)
        out = Mish()(out)
        out = self.conv3(out)
        out = self.bn(out) + x
        return out#Mish()(out)


class ResidualBlock2(nn.Module):
    def __init__(self, chnls, inner_chnnls=None):
        super(ResidualBlock2, self).__init__()
        if inner_chnnls is None:
            inner_chnnls = chnls
        mid_chnnls = chnls // 2
        self.conv1 = nn.Conv2d(in_channels=chnls, out_channels=mid_chnnls*4, kernel_size=5, padding=2)
        self.conv2 = nn.Conv2d(in_channels=chnls, out_channels=mid_chnnls*3, kernel_size=1)
        self.conv3 = nn.Conv2d(in_channels=mid_chnnls*4, out_channels=chnls, kernel_size=1)
        self.bn = nn.BatchNorm2d(chnls)

    def forward(self, x):
        out = self.conv1(x)
        #out = self.conv2(out)
        out = Mish()(out)
        out = self.conv3(out)
        out = self.bn(out) + x
        return out#Mish()(out)


class ResidualBlock3(nn.Module):
    def __init__(self, chnls, inner_chnnls=None):
        super(ResidualBlock3, self).__init__()
        if inner_chnnls is None:
            inner_chnnls = chnls
        self.conv1 =  nn.Conv2d(in_channels=chnls, out_channels=chnls, kernel_size=3, padding=2, dilation=2)
        self.conv2 =  nn.Conv2d(in_channels=chnls, out_channels=chnls*2, kernel_size=1)
        self.conv3 = nn.Conv2d(in_channels=chnls*2, out_channels=chnls, kernel_size=1)
        self.bn = nn.BatchNorm2d(chnls)

    def forward(self, x):
        out = self.conv1(x)
        out = self.conv2(out)
        out = Mish()(out)
        out = self.conv3(out)
        out = self.bn(out) + x
        return out#Mish()(out)


class PEModule(nn.Module):
    def __init__(self, height, width, reduction=16):
        super(PEModule, self).__init__()
        self.height = height
        self.width = width
        self.reduction = reduction
        self.flat_dim = height * width

        ## Fully connected layers
        self.fc1 = nn.Linear(self.flat_dim, self.flat_dim // reduction)
        self.fc2 = nn.Linear(self.flat_dim // reduction, self.flat_dim)

    def forward(self, x):
        batch_size, channels, height, width = x.size()
        device = x.device  # Ensure all operations are on the same device
        x = x.to(device)
        ## Ensure the input feature map has the expected shape
        #assert height == self.height and width == self.width, "Input height and width must match the initialized values"

        ## Global average pooling along channel dimension
        avg_pool = torch.mean(x, dim=1, keepdim=True)  # Shape: (batch_size, 1, height, width)

        # Flatten and fully connected layers
        avg_pool_flat = avg_pool.view(batch_size, self.flat_dim)  # Shape: (batch_size, height * width)

        # Print debug information
        #print(f"avg_pool_flat shape: {avg_pool_flat.shape}")
        #self.fc1 = self.fc1.to(avg_pool_flat.device)
        #self.fc2 = self.fc2.to(avg_pool_flat.device)
        device = avg_pool_flat.device
        avg_pool_flat = avg_pool_flat.to(device)
        ##self.fc1 = self.fc1.to(device)
        #self.fc2 = self.fc2.to(device)
        #y = self.fc1(avg_pool_flat)
        excitation = F.relu(self.fc1(avg_pool_flat))  # Shape: (batch_size, flat_dim // reduction)
        excitation = self.fc2(excitation)  # Shape: (batch_size, flat_dim)

        ## Apply softmax to calculate weights
        excitation = F.softmax(excitation, dim=1)  # Shape: (batch_size, flat_dim)

        ## Reshape to (batch_size, 1, height, width)
        excitation = excitation.view(batch_size, 1, height, width)

        # Re-weighting the input feature map
        x = x * excitation + x

        return x




class HMS_APE_LM(nn.Module):
    def __init__(self, in_chnls, layer):
        super(HMS_APE_LM, self).__init__()
        drop_rate = 0.05
        if layer == 0:
            self.w = 56
            self.h = 56
        else:
            self.w = 56 // (2 * layer)
            self.h = 56 // (2 * layer)


        self.PEM = PEModule(self.h, self.w)
        self.layer_num = layer
        self.ch = in_chnls // 4
        self.dsample = BN_Conv_Mish(2 * in_chnls, in_chnls, 3, 1, 1)
        self.trans_0 = Conv1x1BN(in_chnls, in_chnls)
        self.trans_1 = BN_Conv_Mish(in_chnls, in_chnls, 1, 1, 0)
        self.blocks1 = ResidualBlock1(in_chnls, in_chnls)
        self.blocks2 = ResidualBlock1(in_chnls // 4, in_chnls // 4)
        self.blocks21 = ResidualBlock2(in_chnls // 4, in_chnls // 4)
        self.blocks22 = ResidualBlock(in_chnls // 4, in_chnls // 4)
        self.first_half = HalfSplit(dim=1, first_half=True)  # 对channels进行切半操作, 第一次分: first_half=True
        self.second_split = HalfSplit(dim=1, first_half=False)  # 返回输入的另外一半channesl，两次合起来才是完整的一份channels
        self.channel_shuffle = ChannelShuffle(1)
        self.drop_path = DropPath(drop_rate) if drop_rate > 0. else nn.Identity()
        self.channel_shuffle = ChannelShuffle(1)
        self.trans_2 = BN_Conv_Mish(self.ch * 12, self.ch * 18, 1, 1, 1)
        self.conv0 = BN_Conv_Mish(self.ch * 18, in_chnls, 1, 1, 0)
        self.conv1 = Conv1x1BN(in_chnls,  in_chnls)
        #self.conv2 = Conv1x1BN(in_chnls//4, in_chnls//4)
        self.SE = SE(self.ch * 18, self.ch * 18)
        self.norm = LayerNorm0(self.ch * 6, eps=1e-6, data_format="channels_first")
        self.relu = nn.ReLU6

    def forward(self, x):
        x = self.trans_0(x)
        x_split = torch.split(x, self.ch, dim=1)
        out = []
        out1= []
        x12 = x_split[1]
        x13 = x_split[2]
        x14 = x_split[3]
        # x11 = self.blocks2(x11)
        x12 = self.blocks21(x12)
        x13 = self.blocks2(x13)
        x14 = self.blocks2(x14)

        y1 = torch.cat([x_split[0], x12], dim=1)
        if self.layer_num < 0:
            y1 = self.PEM(y1)
        out.append(y1)
        y1 = torch.cat([x13, x14], dim=1)
        if self.layer_num < 0:
           y1 = self.PEM(y1)
        out.append(y1)
        x13 = x12 + x13
        # x12 =  self.blocks21(x12)
        x13 = self.blocks22(x13)
        x14 = self.blocks2(x14)
        # out1.append(x12)
        y1 = torch.cat([x13, x14], dim=1)
        if self.layer_num < 0:
           y1 = self.PEM(y1)
        out1.append(y1)

        x13 = self.blocks22(x13)
        x14 = x13 + x14
        x14 = self.blocks22(x14)
        y1 = torch.cat([x13, x14], dim=1)
        if self.layer_num < 0:
           y1 = self.PEM(y1)
        out1.append(y1)
        out = torch.cat(out, dim=1)
        out1= torch.cat(out1, dim=1)
        out = out+out1

        out = out + x
        #out = self.trans_1(out)
        out = self.drop_path(out)
        return out


class HMS_APE_L(nn.Module):
    def __init__(self, in_chnls,layer):
        super(HMS_APE_L, self).__init__()
        drop_rate = 0.05
        if layer == 0:
            self.w = 56
            self.h = 56
        else:
            self.w = 56 // (2 * layer)
            self.h = 56 // (2 * layer)

        self.PEM = PEModule(self.h, self.w)
        self.layer_num = layer
        self.ch = in_chnls // 4
        self.dsample = BN_Conv_Mish(2 * in_chnls, in_chnls, 3, 1, 1)
        self.trans_0 = Conv1x1BN(in_chnls, in_chnls)
        self.trans_1 = BN_Conv_Mish(in_chnls, in_chnls, 1, 1, 0)
        self.blocks0 = ResidualBlock1(in_chnls , in_chnls )
        self.blocks1 = ResidualBlock(in_chnls//2, in_chnls//2)
        self.blocks2 = ResidualBlock1(in_chnls // 4, in_chnls // 4)
        self.blocks21 = ResidualBlock2(in_chnls // 4, in_chnls // 4)
        self.blocks22 = ResidualBlock(in_chnls // 4, in_chnls // 4)
        self.first_half = HalfSplit(dim=1, first_half=True)  # 对channels进行切半操作, 第一次分: first_half=True
        self.second_split = HalfSplit(dim=1, first_half=False)  # 返回输入的另外一半channesl，两次合起来才是完整的一份channels
        self.channel_shuffle = ChannelShuffle(1)
        self.drop_path = DropPath(drop_rate) if drop_rate > 0. else nn.Identity()
        self.channel_shuffle = ChannelShuffle(1)
        self.trans_2 = BN_Conv_Mish(self.ch * 8, self.ch * 16, 1, 1, 1)
        self.conv0 = BN_Conv_Mish(self.ch * 16, in_chnls, 1, 1, 0)
        self.conv1 = Conv1x1BN(2 *in_chnls, 2 *in_chnls)
        self.SE = SE(self.ch * 16, self.ch * 16)
        self.norm = LayerNorm0(self.ch * 4, eps=1e-6, data_format="channels_first")
        self.relu = nn.ReLU6
    def forward(self, x,y):
        x = self.trans_0(x)
        x_split = torch.split(x, self.ch, dim=1)
        out = []
        out1 = []
        x12 = x_split[1]
        x13 = x_split[2]
        x14 = x_split[3]
        # x11 = self.blocks2(x11)
        x12 = self.blocks21(x12)
        x13 = self.blocks2(x13)
        x14 = self.blocks2(x14)

        y1 = torch.cat([x_split[0], x12], dim=1)
        if self.layer_num < 0:
            y1 = self.PEM(y1)
        out.append(y1)
        y1 = torch.cat([x13, x14], dim=1)
        if self.layer_num < 0:
           y1 = self.PEM(y1)
        out.append(y1)
        if self.layer_num<3:
           x13 = x12 + x13
        # x12 =  self.blocks21(x12)
        x13 = self.blocks22(x13)
        x14 = self.blocks2(x14)
        #out1.append(x12)
        y1 = torch.cat([x13, x14], dim=1)
        if self.layer_num < 0:
           y1 = self.PEM(y1)
        out1.append(y1)

        x13 = self.blocks2(x13)
        if self.layer_num < 3:
           x14 = x13+x14
        x14 = self.blocks22(x14)
        y1 = torch.cat([x13, x14], dim=1)
        if self.layer_num < 0:
           y1 = self.PEM(y1)
        out1.append(y1)

        y = self.blocks0(y)
        out = torch.cat(out, dim=1)
        out1 = torch.cat(out1, dim=1)
        out = out+out1
        out = self.trans_1(out)
        out = out+x
        #out = self.conv1(out)
        x3 = torch.cat([out, y], dim=1)
        #x3 = self.blocks3(x3)
        x3 = self.trans_2(x3)
        x3 = self.SE(x3)
        #x3 = self.trans_4(x3)
        x3 = self.conv0(x3)
        x3 = self.norm(x3)
        # x3 = out + out1
        x3 = self.channel_shuffle(x3)
        # x3 = self.trans_0(x3)
        x3 = self.drop_path(x3)
        return x3


class Global_pool(nn.Module):
    def __init__(self, in_chnls,out_chnls):
        super(Global_pool, self).__init__()
        drop_rate = 0.1

        self.trans_0 = BN_Conv_Mish(in_chnls, out_chnls , 1, 1, 0)

        self.channel_shuffle = ChannelShuffle(1)

    def forward(self, x):
        #x = torch.cat([x, y], dim=1)
        x = self.trans_0(x)
        #x = self.channel_shuffle(x)
        return x

# 定义h-swith激活函数
class HardSwish(nn.Module):
    def __init__(self, inplace=True):
        super(HardSwish, self).__init__()
        self.relu6 = nn.ReLU6(inplace)

    def forward(self, x):
        return x*self.relu6(x+3)/6

# DW卷积
def ConvBNActivation(in_channels,out_channels,kernel_size,stride,activate):
    # 通过设置padding达到当stride=2时，hw减半的效果。此时不与kernel_size有关，所实现的公式为: padding=(kernel_size-1)//2
    # 当kernel_size=3,padding=1时: stride=2 hw减半, stride=1 hw不变
    # 当kernel_size=5,padding=2时: stride=2 hw减半, stride=1 hw不变
    # 从而达到了使用 stride 来控制hw的效果， 不用去关心kernel_size的大小，控制单一变量
    return nn.Sequential(
            nn.Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel_size, stride=stride, padding=(kernel_size-1)//2, groups=in_channels),
            nn.BatchNorm2d(out_channels),
            nn.ReLU6(inplace=True) if activate == 'relu' else HardSwish()
        )

# PW卷积(接全连接层)
def Conv1x1BN(in_channels,out_channels):
    return nn.Sequential(
            nn.Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=1, stride=1),
            nn.BatchNorm2d(out_channels)
        )

# 普通的1x1卷积
def Conv1x1BNActivation(in_channels,out_channels,activate):
    return nn.Sequential(
            nn.Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=1, stride=1),
            nn.BatchNorm2d(out_channels),
            nn.ReLU6(inplace=True) if activate == 'relu' else HardSwish()
        )


class SE(nn.Module):
    def __init__(self, c1, c2, ratio=16):
        super(SE, self).__init__()
        self.avgpool = nn.AdaptiveAvgPool2d(1)
        self.l1 = nn.Linear(c1, c1 // ratio, bias=False)
        self.relu = nn.ReLU(inplace=True)
        self.l2 = nn.Linear(c1 // ratio, c1, bias=False)
        self.sig = nn.Sigmoid()

    def forward(self, x):
        b, c, _, _ = x.size()
        y = self.avgpool(x).view(b, c)
        y = self.l1(y)
        y = self.relu(y)
        y = self.l2(y)
        y = self.sig(y)
        y = y.view(b, c, 1, 1)
        return x * y.expand_as(x)

class SEInvertedBottleneck(nn.Module):
    def __init__(self, in_channels, mid_channels, out_channels, kernel_size, stride, activate,layer_num):
        super(SEInvertedBottleneck, self).__init__()
        #self.stride = stride
        #self.use_se = use_se
        self.in_channels = in_channels
        self.out_channels = out_channels
        # mid_channels = (in_channels * expansion_factor)

        # 普通1x1卷积升维操作
        self.conv = Conv1x1BNActivation(in_channels, mid_channels,activate)

        # DW卷积 维度不变，但可通过stride改变尺寸 groups=in_channels
        self.depth_conv = ConvBNActivation(mid_channels, mid_channels, kernel_size,stride,activate)

        self.HMS_APE = HMS_APE_LM(mid_channels,layer_num)

        # 注意力机制的使用判断
        #self.C2fV = C2fv2(mid_channels, layer_num)
        self.SE = SE(mid_channels, mid_channels)

        # PW卷积 降维操作
        self.point_conv = Conv1x1BNActivation(mid_channels, out_channels,activate)

    def forward(self, x):
        # DW卷积
        out = self.depth_conv(self.conv(x))
        # 当 use_se=True 时使用注意力机制
        out = self.HMS_APE(out)
        # PW卷积
        out = self.point_conv(out)
        # 残差操作
        # 第一种: 只看步长，步长相同shape不一样的输入输出使用1x1卷积使其相加
        # out = (out + self.shortcut(x)) if self.stride == 1 else out
        # 第二种: 同时满足步长与输入输出的channel, 不使用1x1卷积强行升维
        out = (out + x)
        return out


########################################################################################################

class WindowAttention(nn.Module):
    def __init__(self, dim, heads, head_dim, shifted, window_size, relative_pos_embedding):
        super().__init__()
        inner_dim = head_dim * heads

        self.heads = heads
        self.scale = head_dim ** -0.5
        self.window_size = window_size
        self.relative_pos_embedding = relative_pos_embedding
        self.shifted = shifted

        if self.shifted:
            displacement = window_size // 2
            self.cyclic_shift = CyclicShift(-displacement)
            self.cyclic_back_shift = CyclicShift(displacement)
            self.upper_lower_mask = nn.Parameter(create_mask(window_size=window_size, displacement=displacement,
                                                             upper_lower=True, left_right=False), requires_grad=False)
            self.left_right_mask = nn.Parameter(create_mask(window_size=window_size, displacement=displacement,
                                                            upper_lower=False, left_right=True), requires_grad=False)

        self.to_qkv = nn.Linear(dim, inner_dim * 3, bias=False)

        if self.relative_pos_embedding:
            self.relative_indices = get_relative_distances(window_size) + window_size - 1
            # 确保 relative_indices 是 long 类型的 tensor
            self.relative_indices = self.relative_indices.long()  # 添加此行
            self.pos_embedding = nn.Parameter(torch.randn(2 * window_size - 1, 2 * window_size - 1))
        else:
            self.pos_embedding = nn.Parameter(torch.randn(window_size ** 2, window_size ** 2))

        self.to_out = nn.Linear(inner_dim, dim)

    def forward(self, x):
        if self.shifted:
            x = self.cyclic_shift(x)

        b, n_h, n_w, _, h = *x.shape, self.heads

        qkv = self.to_qkv(x).chunk(3, dim=-1)
        nw_h = n_h // self.window_size
        nw_w = n_w // self.window_size

        q, k, v = map(
            lambda t: rearrange(t, 'b (nw_h w_h) (nw_w w_w) (h d) -> b h (nw_h nw_w) (w_h w_w) d',
                                h=h, w_h=self.window_size, w_w=self.window_size), qkv)

        dots = einsum('b h w i d, b h w j d -> b h w i j', q, k) * self.scale

        if self.relative_pos_embedding:
            # 确保 relative_indices 已经被转换为 long 类型了
            dots += self.pos_embedding[self.relative_indices[:, :, 0], self.relative_indices[:, :, 1]]
        else:
            dots += self.pos_embedding

        if self.shifted:
            dots[:, :, -nw_w:] += self.upper_lower_mask
            dots[:, :, nw_w - 1::nw_w] += self.left_right_mask

        attn = dots.softmax(dim=-1)

        out = einsum('b h w i j, b h w j d -> b h w i d', attn, v)
        out = rearrange(out, 'b h (nw_h nw_w) (w_h w_w) d -> b (nw_h w_h) (nw_w w_w) (h d)',
                        h=h, w_h=self.window_size, w_w=self.window_size, nw_h=nw_h, nw_w=nw_w)
        out = self.to_out(out)

        if self.shifted:
            out = self.cyclic_back_shift(out)
        return out
#################################################################################
class WindowAttention3(nn.Module):
    def __init__(self, dim, heads, head_dim, shifted, window_size, relative_pos_embedding):
        super().__init__()
        inner_dim = head_dim * heads

        self.heads = heads
        self.scale = head_dim ** -0.5
        self.window_size = window_size
        self.relative_pos_embedding = relative_pos_embedding
        self.shifted = shifted
        self.first_half = HalfSplit(dim=3, first_half=True)  # 对channels进行切半操作, 第一次分: first_half=True
        self.second_split = HalfSplit(dim=3, first_half=False)  # 返回输入的另外一半channesl，两次合起来才是完整的一份channels
        if self.shifted:
            displacement = window_size // 2
            self.cyclic_shift = CyclicShift(-displacement)
            self.cyclic_back_shift = CyclicShift(displacement)
            self.upper_lower_mask = nn.Parameter(create_mask(window_size=window_size, displacement=displacement,
                                                             upper_lower=True, left_right=False), requires_grad=False)
            self.left_right_mask = nn.Parameter(create_mask(window_size=window_size, displacement=displacement,
                                                            upper_lower=False, left_right=True), requires_grad=False)

        self.to_qkv = nn.Linear(dim, inner_dim * 3, bias=False)

        if self.relative_pos_embedding:
            self.relative_indices = get_relative_distances(window_size) + window_size - 1
            # 确保 relative_indices 是 long 类型的 tensor
            self.relative_indices = self.relative_indices.long()  # 添加此行
            self.pos_embedding = nn.Parameter(torch.randn(2 * window_size - 1, 2 * window_size - 1))
        else:
            self.pos_embedding = nn.Parameter(torch.randn(window_size ** 2, window_size ** 2))

        self.to_out = nn.Linear(inner_dim, dim)

    def forward(self, x):
        x1=self.first_half(x)
        y=self.second_split(x)

        if self.shifted:
            x1 = self.cyclic_shift(x1)

        b, n_h, n_w, _, h = *x1.shape, self.heads

        qkv = self.to_qkv(x1).chunk(3, dim=-1)
        nw_h = n_h // self.window_size
        nw_w = n_w // self.window_size

        q, k, v = map(
            lambda t: rearrange(t, 'b (nw_h w_h) (nw_w w_w) (h d) -> b h (nw_h nw_w) (w_h w_w) d',
                                h=h, w_h=self.window_size, w_w=self.window_size), qkv)

        if self.shifted:
            y = self.cyclic_shift(y)
        b, n_h, n_w, _, h = *y.shape, self.heads

        qkv = self.to_qkv(y).chunk(3, dim=-1)
        nw_h = n_h // self.window_size
        nw_w = n_w // self.window_size

        q1, k1, v1 = map(
            lambda t: rearrange(t, 'b (nw_h w_h) (nw_w w_w) (h d) -> b h (nw_h nw_w) (w_h w_w) d',
                                h=h, w_h=self.window_size, w_w=self.window_size), qkv)




        dots = einsum('b h w i d, b h w j d -> b h w i j', q, k) * self.scale
        dots1 =einsum('b h w i d, b h w j d -> b h w i j', q1, k1) * self.scale
#############
        if self.relative_pos_embedding:
            # 确保 relative_indices 已经被转换为 long 类型了
            dots += self.pos_embedding[self.relative_indices[:, :, 0], self.relative_indices[:, :, 1]]
        else:
            dots += self.pos_embedding

        if self.shifted:
            dots[:, :, -nw_w:] += self.upper_lower_mask
            dots[:, :, nw_w - 1::nw_w] += self.left_right_mask

        attn = dots.softmax(dim=-1)
#############
        if self.relative_pos_embedding:
            # 确保 relative_indices 已经被转换为 long 类型了
            dots1 += self.pos_embedding[self.relative_indices[:, :, 0], self.relative_indices[:, :, 1]]
        else:
            dots1 += self.pos_embedding

        if self.shifted:
            dots1[:, :, -nw_w:] += self.upper_lower_mask
            dots1[:, :, nw_w - 1::nw_w] += self.left_right_mask

        attn1 = dots1.softmax(dim=-1)
###################

        out = einsum('b h w i j, b h w j d -> b h w i d', attn, v)
        out = einsum('b h w i j, b h w j d -> b h w i d', attn1, out)
        out = rearrange(out, 'b h (nw_h nw_w) (w_h w_w) d -> b (nw_h w_h) (nw_w w_w) (h d)',
                        h=h, w_h=self.window_size, w_w=self.window_size, nw_h=nw_h, nw_w=nw_w)
        out = self.to_out(out)
        out = out + y
        if self.shifted:
            out = self.cyclic_back_shift(out)
        return out
#########################################################################################


########################################################################################
class SwinBlock(nn.Module):
    def __init__(self, dim, heads, head_dim, mlp_dim, shifted, window_size, relative_pos_embedding,layers,layer_num):
        super().__init__()

        self.attention_block = PreNorm2(dim, WindowAttention3(dim=dim,
                                                              heads=heads,
                                                              head_dim=head_dim,
                                                              shifted=shifted,
                                                              window_size=window_size,
                                                              relative_pos_embedding=relative_pos_embedding))
        self.attention_block1 = Residual(PreNorm(dim, WindowAttention(dim=dim,
                                                                     heads=heads,
                                                                     head_dim=head_dim,
                                                                     shifted=shifted,
                                                                     window_size=window_size,
                                                                     relative_pos_embedding=relative_pos_embedding)))
        self.mlp_block = Residual(PreNorm(dim, FeedForward(dim=dim, hidden_dim=mlp_dim)))
        self.first_half = HalfSplit(dim=1, first_half=True)  # 对channels进行切半操作, 第一次分: first_half=True
        self.second_split = HalfSplit(dim=1, first_half=False)  # 返回输入的另外一半channesl，两次合起来才是完整的一份channelsself
        self.layer = layers

        self.HMS_APE =  HMS_APE_LM(dim,layer_num)
        self.dsample = BN_Conv_Mish(dim, dim//2, 3, 1, 1)
        self.trans_0 = BN_Conv_Mish(dim, dim, 1, 1, 0)
        self.drop_path = DropPath(0.1)
    def forward(self, x,y,i):
        if i != 1 and self.layer > 2:
           x1 = self.attention_block1(x)
        else:
           y_p = y.permute(0, 2, 3, 1)
           xy = torch.cat([x, y_p], dim=3)
           x1 = self.attention_block(xy)+x
           y = self.HMS_APE(y)
        x1 = self.mlp_block(x1)
        x1 = self.drop_path(x1)
        return x1,y


class SwinBlock1(nn.Module):
    def __init__(self, dim, heads, head_dim, mlp_dim, shifted, window_size, relative_pos_embedding,layers,layer_num):
        super().__init__()

        self.attention_block = Residual(PreNorm(dim, WindowAttention(dim=dim,
                                                                     heads=heads,
                                                                     head_dim=head_dim,
                                                                     shifted=shifted,
                                                                     window_size=window_size,
                                                                     relative_pos_embedding=relative_pos_embedding)))
        self.mlp_block = Residual(PreNorm(dim, FeedForward(dim=dim, hidden_dim=mlp_dim)))
        self.first_half = HalfSplit(dim=1, first_half=True)  # 对channels进行切半操作, 第一次分: first_half=True
        self.second_split = HalfSplit(dim=1, first_half=False)  # 返回输入的另外一半channesl，两次合起来才是完整的一份channelsself
        self.HMS_APE = HMS_APE_L(dim,layer_num)

        self.layer = layers
        self.mutate = mutate(dim,dim)
        self.dsample = BN_Conv_Mish(dim, dim, 3, 1, 1)
        self.trans_0 = BN_Conv_Mish(dim, dim, 1, 1, 0)
        self.drop_path = DropPath(0.1)
    def forward(self, x,y,i):
        x = self.attention_block(x)
        if i !=1 and self.layer >2:
           y = y
        else:
           x1 = self.mutate(x)
           y = self.HMS_APE(y, x1)
        x = self.mlp_block(x)
        x = self.drop_path(x)
        return x,y


class mutate(nn.Module):
    def __init__(self, in_channels, out_channels):
        super().__init__()
        self.trans_0 = BN_Conv_Mish(in_channels, out_channels, 1, 1, 0)

    def forward(self, x):

        x = x.permute(0, 3, 1, 2)
        x = self.trans_0(x)

        return x

class Remutate(nn.Module):
    def __init__(self, in_channels, out_channels):
        super().__init__()
        self.trans_0 = BN_Conv_Mish(in_channels, out_channels, 1, 1, 0)

    def forward(self, x):
        x =  x.permute(0, 2, 3, 1)
        x = self.trans_0(x)
        return x

class PatchMerging(nn.Module):
    def __init__(self, in_channels, out_channels, downscaling_factor):
        super().__init__()
        self.downscaling_factor = downscaling_factor
        self.patch_merge = nn.Unfold(kernel_size=downscaling_factor, stride=downscaling_factor, padding=0)
        self.linear = nn.Linear(in_channels * downscaling_factor ** 2, out_channels)

    def forward(self, x):
        b, c, h, w = x.shape
        new_h, new_w = h // self.downscaling_factor, w // self.downscaling_factor
        x = self.patch_merge(x).view(b, -1, new_h, new_w).permute(0, 2, 3, 1)
        x = self.linear(x)
        return x


class StageModule(nn.Module):
    def __init__(self, in_channels, hidden_dimension, layers, downscaling_factor, num_heads,st, head_dim, window_size,
                 relative_pos_embedding,layer_num):
        super().__init__()

        self.dsample =  Downsample(in_channels,hidden_dimension,st)

        assert layers % 2 == 0, 'Stage layers need to be divisible by 2 for regular and shifted block.'

        self.patch_partition = PatchMerging(in_channels=in_channels, out_channels=hidden_dimension,
                                            downscaling_factor=downscaling_factor)

        self.layers = nn.ModuleList([])
        self.first_half = HalfSplit(dim=1, first_half=True)  # 对channels进行切半操作, 第一次分: first_half=True
        self.second_split = HalfSplit(dim=1, first_half=False)  # 返回输入的另外一半channesl，两次合起来才是完整的一份channelsself
        for _ in range(layers // 2):
            self.layers.append(nn.ModuleList([
                SwinBlock1(dim=hidden_dimension, heads=num_heads, head_dim=head_dim, mlp_dim=hidden_dimension * 4,
                          shifted=False, window_size=window_size, relative_pos_embedding=relative_pos_embedding,layers =layers,layer_num=layer_num),
                SwinBlock(dim=hidden_dimension, heads=num_heads, head_dim=head_dim, mlp_dim=hidden_dimension * 4,
                          shifted=True, window_size=window_size, relative_pos_embedding=relative_pos_embedding,layers =layers,layer_num=layer_num),
            ]))

    def forward(self, x):
        x1 = self.first_half(x)  # # 一开始直接将channels等分两半，x1称为主分支的一半，
        y = self.second_split(x)  # x2称为输入的另外一半channels
        x1 = self.patch_partition(x1)
        y = self.dsample(y)
        #y = self.C2fS(y)
        #y = self.blocks(y)
        for i, (regular_block, shifted_block) in enumerate(self.layers):
            x1, y = regular_block(x1, y,i)
            x1, y = shifted_block(x1, y,i)
            #print(f"当前循环次数: {i}")
        x = torch.cat([x1.permute(0, 3, 1, 2), y], dim=1)
        return x

class StageModule1(nn.Module):
    def __init__(self, in_channels, hidden_dimension, layers, downscaling_factor, num_heads,st, head_dim, window_size,
                 relative_pos_embedding,layer_num):
        super().__init__()

        self.dsample =  Downsample(3,hidden_dimension,st)

        assert layers % 2 == 0, 'Stage layers need to be divisible by 2 for regular and shifted block.'

        self.patch_partition = PatchMerging(in_channels=in_channels, out_channels=hidden_dimension,
                                            downscaling_factor=downscaling_factor)

        self.layers = nn.ModuleList([])

        for _ in range(layers // 2):
            self.layers.append(nn.ModuleList([
                SwinBlock1(dim=hidden_dimension, heads=num_heads, head_dim=head_dim, mlp_dim=hidden_dimension * 4,
                          shifted=False, window_size=window_size, relative_pos_embedding=relative_pos_embedding,layers =layers,layer_num=layer_num),
                SwinBlock(dim=hidden_dimension, heads=num_heads, head_dim=head_dim, mlp_dim=hidden_dimension * 4,
                          shifted=True, window_size=window_size, relative_pos_embedding=relative_pos_embedding,layers =layers,layer_num=layer_num),
            ]))

    def forward(self, x,y):

        x1 = self.patch_partition(x)
        y = self.dsample(y)
        #y = self.C2fS(y)
        #y = self.blocks(y)
        for i, (regular_block, shifted_block) in enumerate(self.layers):
            x1, y = regular_block(x1, y,i)
            x1, y = shifted_block(x1, y,i)
            #print(f"当前循环次数: {i}")
        x = torch.cat([x1.permute(0, 3, 1, 2), y], dim=1)
        return x



class HMCformer(nn.Module):
    def __init__(self, *, hidden_dim, layers, heads, st, channels=3, num_classes=1000, head_dim=32, window_size=7,
                 downscaling_factors=(4, 2, 2, 2), relative_pos_embedding=True):
        super().__init__()
        self.second_split = HalfSplit(dim=1, first_half=False)  # 返回输入的另外一半channesl，两次合起来才是完整的一份channelsself
        self.first_half = HalfSplit(dim=1, first_half=True)  # 对channels进行切半操作, 第一次分: first_half=True
        self.layer_num = 0
        self.stage1 = StageModule1(in_channels=channels, hidden_dimension=hidden_dim, layers=layers[0],
                                  downscaling_factor=downscaling_factors[0], num_heads=heads[0], st=st[0], head_dim=head_dim,
                                  window_size=window_size, relative_pos_embedding=relative_pos_embedding,layer_num=self.layer_num )
        self.stage2 = StageModule(in_channels=hidden_dim, hidden_dimension=hidden_dim * 2, layers=layers[1],
                                  downscaling_factor=downscaling_factors[1], num_heads=heads[1], st=st[1],head_dim=head_dim,
                                  window_size=window_size, relative_pos_embedding=relative_pos_embedding,layer_num=self.layer_num + 1)
        self.stage3 = StageModule(in_channels=hidden_dim *2, hidden_dimension=hidden_dim * 4, layers=layers[2],
                                  downscaling_factor=downscaling_factors[2], num_heads=heads[2], st=st[2],head_dim=head_dim,
                                  window_size=window_size, relative_pos_embedding=relative_pos_embedding,layer_num=self.layer_num + 2)
        self.stage4 = StageModule(in_channels=hidden_dim * 4, hidden_dimension=hidden_dim * 8, layers=layers[3],
                                  downscaling_factor=downscaling_factors[3], num_heads=heads[3], st=st[3], head_dim=head_dim,
                                  window_size=window_size, relative_pos_embedding=relative_pos_embedding,layer_num=self.layer_num + 3)
        self.SEblock1 = SEInvertedBottleneck(in_channels=hidden_dim * 2, mid_channels=hidden_dim * 4,
                                             out_channels=hidden_dim * 2, kernel_size=3, stride=1, activate='hswish',
                                             layer_num=self.layer_num + 1)
        self.SEblock2 = SEInvertedBottleneck(in_channels=hidden_dim * 4, mid_channels=hidden_dim * 8,
                                             out_channels=hidden_dim * 4, kernel_size=3, stride=1, activate='hswish',
                                             layer_num=self.layer_num + 2)
        self.pool = Global_pool(hidden_dim * 16,hidden_dim * 16)
        self.mlp_head = nn.Sequential(
            nn.LayerNorm(hidden_dim * 16),
            nn.Linear(hidden_dim * 16, num_classes)
        )

        self.saved_outputsx = []  # List to save the outputs
        self.saved_outputsy = []  # List to save the outputs
    def forward(self, img):


        x = self.stage1(img,img)
        x = self.stage2(x)
        x1 = self.first_half(x)
        y = self.second_split(x)
        y = self.SEblock1(y)
        x = torch.cat([x1, y], dim=1)
        x = self.stage3(x)
        x1 = self.first_half(x)

        y = self.second_split(x)
        y = self.SEblock2(y)
        x = torch.cat([x1, y], dim=1)
        x = self.stage4(x)

        x = self.pool(x)
        x = x.mean(dim=[2, 3])

        x = self.mlp_head(x)
        return x


def HMCF_Xs(hidden_dim=36, layers=(2, 2, 6, 2), heads=(3, 6, 12, 24), st=(4,2,2,2), **kwargs):
    return HMCformer(hidden_dim=hidden_dim, layers=layers, heads=heads, st = st, **kwargs)



